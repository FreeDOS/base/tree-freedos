@REM @ECHO OFF
%1 %2
pause
%1 %2 /F
pause
%1 %2 /DA
pause
%1 %2 /DA /F
pause
%1 %2 /DA /F /A
pause
@ECHO This should fail with invalid switch /DS when tree compiled by Micro-C
%1 %2 /DA /F /DS
pause
@ECHO This should fail with invalid switch /DS when tree compiled by Micro-C
%1 %2 /DA /F /DS /A
pause
@ECHO This is a test of an invalid argument
%1 %2 /DD
