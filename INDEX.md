# Tree

Graphically displays the folder structure of a drive or path.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## TREE.LSM

<table>
<tr><td>title</td><td>Tree</td></tr>
<tr><td>version</td><td>3.7.3</td></tr>
<tr><td>entered&nbsp;date</td><td>2024-02-17</td></tr>
<tr><td>description</td><td>Graphically displays the folder structure of a drive or path.</td></tr>
<tr><td>keywords</td><td>tree, FreeDOS, DOS, Win32</td></tr>
<tr><td>author</td><td>Dave Dunfield &lt;dave@dunfield.com&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Kenneth J. Davis &lt;jeremyd@fdos.org&gt;</td></tr>
<tr><td>primary&nbsp;site</td><td>http://www.fdos.org</td></tr>
<tr><td>alternate&nbsp;site</td><td>http://www.ibiblio.org/pub/micro/pc-stuff/freedos/files/dos/tree/</td></tr>
<tr><td>original&nbsp;site</td><td>http://www.dunfield.com</td></tr>
<tr><td>http</td><td>//www.darklogic.org/fdos/projects/tree/</td></tr>
<tr><td>platforms</td><td>DOS (TC2, TC++1, TC30, BC31, BC45, MicroC v3*, Pacific C*, Digital Mars),</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GPL](LICENSE)</td></tr>
</table>
